'''
Crea la lista delle funzioni definite nel file testfunzioni.tex.

Crea la lista della chiamate delle funzioni presenti nel file testfunzioni.tex.
'''


def load_functions(funct_file):
    """Return list of grafics functions in g_f file."""
    result = []
    with open(funct_file) as gf: 
        for line in gf: 
            inewcommand = line.find(r'\newcommand{')
            if inewcommand >= 0 and inewcommand < line.find(r'%'):
                endname = line.find('}')
                numpar = 0
                if line[endname+1] == '[':
                    numpar = int(line[endname+2])
                result.append((line[12:endname], numpar))
                
    return result

funclist = load_functions('test/testfunzioni.tex')

for e in funclist:
    print(e)

def functioncalls1(tex_file):
    """Return functions calls
works correctly only for functions without parameters."""
    result = []
    with open(tex_file, 'r') as file:
        textext = file.read()
        for funct in funclist:
            if funct[0] in textext:
                result.append(funct[0])
    return result
            
    
functions = functioncalls1('test/testfunzioni.tex')

for e in functions:
    print(e)
