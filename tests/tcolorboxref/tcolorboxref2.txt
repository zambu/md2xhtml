% !TEX encoding = UTF-8 Unicode
%--8<-------------------------------------------------------
\documentclass[10pt,a4paper]{memoir}
\usepackage[T1]{fontenc} 
% \usepackage[utf8]{inputenc}    Non va bene con LuaLaTeX !!!
\usepackage[italian]{babel} 
\usepackage{amsmath, amssymb, amsthm}
\usepackage{ulem}
\RequirePackage[colorlinks, hyperindex, pagebackref,
                linkcolor=blue, filecolor=blue,
                citecolor = black, urlcolor=blue]{hyperref}

\usepackage{tcolorbox}
\tcbuselibrary{most}
\usepackage{lipsum}

\theoremstyle{plain}

\newcommand{\coloross}{violet}
\newcommand{\colortcblist}{green}

\DeclareTCBListing{latexlistingbox}{ s O{} m }{%
colback = \colortcblist!5!white,
colframe = \colortcblist!75!black,
fonttitle = \bfseries,
IfBooleanTF = {#1}{}{listing side text},
title={#3},#2}

\tcbset{
ossstyle/.style={fonttitle=\bfseries\upshape, fontupper=\slshape,
arc=0mm, colback=\coloross!5!white, colframe=\coloross!50!black,
theorem style=plain, coltitle=\coloross!30!black}
}

\newtcbtheorem[number within=chapter]{osservazione}{Osservazione}
              {ossstyle}{oss}

\begin{document}

\chapter{Problem with tcolorbox references}

\paragraph{Osservazioni}
Due ambienti per le osservazioni.

\begin{osservazione}{Osservazione interessante}{osser}
Questo è il contenuto di una osservazione.
\end{osservazione}

Nel testo può esserci un riferimento all'osservazione: \\
osservazione numero: \ref{oss:osser};\\
titolo dell'osservazione: \nameref{oss:osser};\\
pagina dell'osservazione: \pageref{oss:osser}.

\begin{latexlistingbox}*{Osservazione}
\begin{osservazione}{Osservazione molto molto interessante}{primaoss}
Questo è il contenuto di un'altra osservazione.
\end{osservazione}

Nel testo può esserci un riferimento all'osservazione: \\
osservazione numero: \ref{oss:primaoss};\\
titolo dell'osservazione: \nameref{oss:primaoss};\\
pagina dell'osservazione: \pageref{oss:primaoss}.
\end{latexlistingbox}

\begin{latexlistingbox}*{Ambiente osservazioni}
\begin{osservazioni}{Raccolta di più osservazioni}{secondaoss}
\begin{itemize} [noitemsep]
\item Prima osservazione.
\item Seconda osservazione.
\end{itemize}
\end{osservazioni}

Nel testo può esserci un riferimento alle osservazioni:\\
Vedi le osservazioni: \ref{oss:secondaoss} \nameref{oss:secondaoss}
a pag. \pageref{oss:secondaoss}.
\end{latexlistingbox}

\end{document}
%--8<-------------------------------------------------------
