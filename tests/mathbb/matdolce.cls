%%
%% This is file `matdolce.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% matdolce.dtx  (with options: `class')
%% 
%% Copyright (C) 2013-2022 by Daniele Zambelli - daniele.zambelli@gmail.com
%% 
\NeedsTeXFormat{LaTeX2e}[[2018/01/01]
\ProvidesClass{matdolceclasse}[%
    2022/03/20 v0.1.6
    Classe per il libro di Matematica Dolce]

\LoadClassWithOptions{memoir}[2011/03/06]

\RequirePackage[italian]{babel}
\RequirePackage{amsmath, amsthm}% amsfonts è caricato da amssymb
\RequirePackage{eurosym} % simbolo dell'euro
\RequirePackage{pifont} % simboli unicode
\RequirePackage[usenames, dvipsnames]{xcolor}   % gestione colori

\RequirePackage[e]{esvect} % per i vettori
\RequirePackage{physics}   % fisica e differenziale (dx)
\RequirePackage{cancel} % per le semplificazioni
\RequirePackage[normalem]{ulem} % sottolineature
\DeclareMathAccent{\widearc}{\mathord}{largesymbols}{216}
\RequirePackage[np,noaddmissingzero,autolanguage]{numprint}
\RequirePackage{mathtools} % da approfondire

\usepackage{iftex}

\ifpdftex\typeout{************* PDFTeX*****************}
  \RequirePackage[T1]{fontenc}
  \RequirePackage[utf8]{inputenc}
  \RequirePackage{stix2} % font
  \usepackage[accsupp]{axessibility} % per la lettura delle formule

\else
  \ifluatex\typeout{************* LuaLaTeX *****************}
    \usepackage{fontspec}
    \setmainfont{STIX}[Ligatures=TeX]
    \usepackage[math-style=ISO]{unicode-math}
    \setmathfont{STIX Two Math}
  \fi
\fi

\AtEndPreamble{\ifcsstring{languagename}{english}{\relax}{%
  \mathcode`\,=\string"8000}%
   \DeclareMathSymbol{\virgola}{\mathpunct}{letters}{"3B}%
   \DeclareMathSymbol{\virgoladecimale}{\mathord}{letters}{"3B}%
}
{\catcode `,=\active \gdef,{\futurelet\let@token\m@thcomma}}%
\AtEndPreamble{%
  \unless\ifcsname m@thcomma\endcsname\providecommand\m@thcomma{}\fi
    \renewcommand\m@thcomma[1]{%
    \let\tempB#1\relax
    \ifx\tempB\bar
      \virgoladecimale
    \else
      \ifx\tempB\overline
        \virgoladecimale
      \else
        \unless\ifcat\noexpand\let@token*%
          \virgola
        \else
          \edef\tempA{\expandafter\@gobble\string#1}%
          \ifx\tempA\@empty
            \virgoladecimale
          \else
            \virgola
          \fi
        \fi
      \fi
    \fi#1%
  }
}

\AtEndPreamble{
  \providecommand\IntelligentComma{}
  \providecommand\NoIntelligentComma{}
  \renewcommand\IntelligentComma{\mathcode`\,=\string"8000}
  \renewcommand\NoIntelligentComma{\mathcode`\,=\string"613B}
}

\ifdefined\HCode                          % compilazione htlatex
  \def\pgfsysdriver{pgfsys-dvisvgm4ht.def}
  \newcommand{\url}[1]{\Link[#1]{}{} #1 \EndLink}
  \newcommand{\href}[2]{\Link[#1]{}{} #2 \EndLink}
  \newcommand{\hypertarget}[2]{\Link[]{}{#1} #2 \EndLink}

\else                                     % compilazione pdflatex
  \RequirePackage[colorlinks, hyperindex, pagebackref,
                  linkcolor=RoyalBlue, filecolor=RoyalBlue,
                  citecolor = black, urlcolor=RoyalBlue]{hyperref}
\fi

\RequirePackage[inline]{enumitem} % elenchi
\RequirePackage{multicol} % testo su più colonne
\RequirePackage{quoting} % per i testi citati
\quotingsetup{font=small}
\RequirePackage{tabto} % per punti di tabulazione
\RequirePackage{microtype}

\RequirePackage{tikz}
\RequirePackage{tkz-fct} % per il disegno di funzioni
\RequirePackage{tikz-qtree} % per alberi
\usetikzlibrary{
  arrows,%
  arrows.meta,
  through,
  automata,%
  backgrounds,%
  calc,%
  decorations.markings,%
  decorations.shapes,%
  decorations.text,%
  decorations.pathreplacing,%
  fit,%
  matrix,%
  mindmap,%
  patterns,%
  positioning,%
  intersections,%
  shapes,%
  shapes.geometric,
}
\RequirePackage[most]{tcolorbox}% per i box colorati

\newcommand{\colormd}{Mahogany}
\newcommand{\colortcblist}{BlueGreen}
\newcommand{\coloross}{violet}
\newcommand{\coloresem}{yellow}
\newcommand{\colorproc}{teal}
\newcommand{\colordef}{blue}
\newcommand{\colortheo}{green}
\newcommand{\colorproof}{orange}
\newcommand{\colortit}{green}

\newcommand{\numnameref}[1]{\ref{#1} \nameref{#1}}

\providecommand{\folder}{./}

\newcommand{\parte}[2]{
  \renewcommand{\folder}{#1}
  \graphicspath{{\folder}}
  \include{\folder #2}
}

\newcommand{\capitolo}[2]{
  \renewcommand{\folder}{#1}
  \graphicspath{{\folder}}
  \include{\folder #2}
  \newpage
  \include{\folder #2_ese}
  \cleardoublepage
}

\newtcolorbox{inibox}[1]{colback=\colormd!5,
colframe=\colormd!75,fonttitle=\bfseries,
colbacktitle=\colormd!75,enhanced,
attach boxed title to top center={yshift=-1mm},
title={#1}}

\newcommand{\inicapitolo}[1]{
\begin{inibox}{In questo capitolo incontrerai:}
#1
\end{inibox}
}

\newenvironment{inaccessibleblock}[1][]{}{}

\newcommand{\spazielenx}{%
  \setlength{\itemsep}{0pt}%
  \setlength{\parskip}{5pt}%
}

\renewcommand{\labelitemi}{\textcolor{Salmon}{\ding{225}}}

\renewcommand{\labelitemii}{%
  \textcolor{Salmon}{\ding{224}}%
}

\renewcommand{\labelitemiii}{%
  \textcolor{Salmon}{\ding{223}}%
}

\newenvironment{enumeratea}{%
  \begin{enumerate}[label=\alph*\,), nosep]%
}{%
  \end{enumerate}%
}

\newtheorem{esercizio}{\color{Sepia}}[chapter]

\ifpdftex
   \lstset{basicstyle=\small, language=Python, showstringspaces=false}
   \lstset{frame=trbl, frameround=ftff}
\fi

\DeclareTCBListing{latexlistingbox}{ s O{} m }{%
colback = \colortcblist!5!white,
colframe = \colortcblist!75!black,
fonttitle = \bfseries,
IfBooleanTF = {#1}{}{listing side text},
title={#3},#2}

\theoremstyle{plain}
\tcbset{
ossstyle/.style={fonttitle=\bfseries\upshape, fontupper=\slshape,
arc=0mm, colback=\coloross!5!white, colframe=\coloross!50!black,
theorem style=plain, coltitle=\coloross!30!black},
esemstyle/.style={fonttitle=\bfseries\upshape, fontupper=\slshape,
arc=0mm, colback=\coloresem!5!white, colframe=\coloresem!50!black,
theorem style=plain, coltitle=\coloresem!30!black},
procstyle/.style={fonttitle=\bfseries\upshape, fontupper=\slshape,
arc=0mm, colback=\colorproc!5!white, colframe=\colorproc!50!black,
theorem style=plain, coltitle=\colortit!30!black},
defstyle/.style={fonttitle=\bfseries\upshape, fontupper=\slshape,
colback=\colordef!5!white, colframe=\colordef!50!black,
theorem style=plain, coltitle=\colortit!30!black},
poststyle/.style={fonttitle=\bfseries\upshape, fontupper=\slshape,
colback=\colortheo!10!white, colframe=\colortheo!75!black,
theorem style=plain, coltitle=\colortit!30!black},
theostyle/.style={fonttitle=\bfseries\upshape, fontupper=\slshape,
colback=\colortheo!5!white, colframe=\colortheo!50!black,
theorem style=plain, coltitle=\colortit!30!black}
}

\tcolorboxenvironment{proof}{% `proof' from `amsthm'
blanker, breakable, left=5mm, oversize,
before skip=10pt, after skip=10pt,
borderline west={1mm}{0pt}{\colorproof!60!black!20!white}}

\newenvironment{dimostrazione}{%
  \begin{proof}[Dimostrazione]%
}{%
  \end{proof}%
}

\newenvironment{soluzione}{%
  \begin{proof}[Soluzione]%
}{%
  \end{proof}%
}


\newtcbtheorem[number within=chapter]{osservazione}{Osservazione}
{ossstyle}{oss}

\newtcbtheorem[use counter from=osservazione]{osservazioni}{Osservazioni}
{ossstyle}{oss}

\newtcbtheorem[number within=chapter]{esempio}{Esempio}
{esemstyle}{esem}

\newtcbtheorem[use counter from=esempio]{esempi}{Esempi}
{esemstyle}{esem}

\newtcbtheorem[use counter from=esempio]{problema}{Problema}
{esemstyle}{esem}

\newtcbtheorem[number within=chapter]{procedura}{Procedura}
{procstyle}{proc}

\newtcbtheorem[number within=chapter]{definizione}{Definizione}
{defstyle}{def}

\newtcbtheorem[number within=chapter]{postulato}{Postulato}
{poststyle}{post}

\newtcbtheorem[number within=chapter]{teorema}{Teorema}
{theostyle}{th}

\newtcbtheorem[use counter from=teorema]{lemma}{Lemma}
{theostyle}{th}

\newtcbtheorem[use counter from=teorema]{corollario}{Corollario}
{theostyle}{th}

\newcommand*{\frntspz}{%
  \newlength{\drop}
  \drop=0.15\textheight
  \vspace{\drop}
  {\centering
    \fontsize{16pt}{0in}%
    \selectfont\MakeUppercase\serie\\[0.5\drop]
    \fontsize{26pt}{0pt}%
    \selectfont\MakeUppercase\titolo\par
    \vspace{\drop}
    {\LARGE\descr}\par
    \vspace{2.5\drop}
    \large\editore
    \vskip2mm
    \large\Edizione~-~\anno\par
    \vspace{\drop}
  }
}

\renewcommand{\printpartname}{}
\renewcommand{\partnamenum}{}
\renewcommand{\printpartnum}{}
\renewcommand{\partnumfont}{%
  \fontseries{b} \fontsize{.9in}{0in} \selectfont \color{\colormd}
}
\renewcommand{\parttitlefont}{
  \Huge\fontseries{b}\fontfamily{phv} \selectfont\raggedleft%
}
\renewcommand{\beforepartskip}{\vspace*{0.1in}}
\renewcommand{\midpartskip}{}
\renewcommand{\afterpartskip}{\vspace{1in}}
\renewcommand{\@setuppart}{\beforepartskip}
\renewcommand{\@endpart}{\afterpartskip}
\renewcommand{\printparttitle}[1]{%
  \thispagestyle{empty}%
    \noindent
    \begin{tabularx}{\textwidth}{Xr}
     {\parbox[b]{\linewidth}{\parttitlefont #1}}%
      & \raisebox{-15pt}{\partnumfont \thepart} \\%
    \end{tabularx}
}

\newif\ifchapternonum
\makechapterstyle{matdolcechap}{%
  \renewcommand\printchaptername{}
  \renewcommand\printchapternum{}
  \renewcommand{\beforechapskip}{-25pt}
  \renewcommand\printchapternonum{\chapternonumtrue}
  \renewcommand\chaptitlefont{%
    \huge\fontseries{b}\fontfamily{phv}\selectfont\raggedleft%
  }
  \renewcommand\chapnumfont{%
    \fontseries{b} \fontsize{.9in}{0in} \selectfont \color{\colormd}
  }
  \renewcommand\printchaptertitle[1]{%
    \noindent%
    \ifchapternonum%
      \begin{tabularx}{\textwidth}{X}%
        {\parbox[b]{\linewidth}{\chaptitlefont ##1}%
        \vphantom{\raisebox{15pt}{\chapnumfont 1}}}
      \end{tabularx}%
    \else
      \begin{tabularx}{\textwidth}{Xl}
        {\parbox[b]{\linewidth}{\chaptitlefont ##1}}%
        & \raisebox{-15pt}{ \chapnumfont  \thechapter}%
      \end{tabularx}%
    \fi
    \par\vskip2mm%\hrule
  }
}
\chapterstyle{matdolcechap}

\setsecheadstyle{%
  \Large\fontfamily{phv}\fontseries{b}\selectfont\raggedright%
}

\setsubsecheadstyle{%
  \fontfamily{phv}\fontseries{b}\selectfont\raggedright%
}

\setsubsubsecheadstyle{%
  \fontfamily{phv}\fontseries{b}\selectfont\raggedright%
}


\makepagestyle{matdolcepage}
\makeevenhead{matdolcepage}
  {\oldstylenums{\thepage} \qquad \slshape\leftmark}{}{}
\makeoddhead{matdolcepage}
  {}{}{\slshape\rightmark \qquad \oldstylenums{\thepage}}
\makeheadrule{matdolcepage}{\textwidth}{\normalrulethickness}
\makeheadfootruleprefix{matdolcepage}{\color{\colormd}}{}
\nouppercaseheads
\newcommand{\headcap}{%
  {\color{\colormd} \thechapter. }%
}
\newcommand{\headsec}{%
  {\color{\colormd} \thesection. }%
}
\makepsmarks{matdolcepage}{%
  \def\chaptermark##1{\markboth{%
        \ifnum \value{secnumdepth} > -1
          \if@mainmatter
           \headcap   %
          \fi
        \fi
        ##1}{}%
  }
  \def\sectionmark##1{\markright{%
        \ifnum \value{secnumdepth} > 0
          \headsec ~%
        \fi
        ##1}%
  }

\createplainmark{toc}{both}{\contentsname}
\createplainmark{lof}{both}{\listfigurename}
\createplainmark{lot}{both}{\listtablename}
\createplainmark{bib}{both}{\bibname}
\createplainmark{index}{both}{\indexname}
\createplainmark{glossary}{both}{\glossaryname}
}

\pagestyle{matdolcepage}

\newcommand{\longarray}{\renewcommand{\arraystretch}{2}}

\newcommand{\osservazionea}{%
  \paragraph{%
    {\color{\colormd}\ding{113}} Osservazione%
  }%
}

\newcommand{\conclusione}{%
  \paragraph{%
    {\color{\colormd}\ding{109}} Conclusione%
  }%
}

\newcommand{\vspazio}{\vspace{1ex}}

\newcommand{\risolvi}{%
  {\fontsize{12pt}{0pt}%
    \Writinghand\,} \emph{Esercizio proposto: %
  }%
}

\newcommand{\risolvii}{%
  {\fontsize{12pt}{0pt}%
    \Writinghand\,} \emph{Esercizi proposti: %
  }%
}

\providecommand{\superscriptREMOVE}[1]{^{#1}}

\newcommand{\Ast}{{}^{\ast}}

\newcommand{\croceREMOVE}{\superscript{\dag}}

\newcommand{\aC}{a.e.v.}

\newcommand{\dC}{p.e.v.}

\newcommand{\aev}{a.e.v.}

\newcommand{\pev}{p.e.v.}

\newcommand{\tggg}[0]{\textgreater\textgreater\textgreater}

\newcommand{\fcharboxREMOVE}[1]{\fbox{#1}}

\newcommand{\emptybox}{\fbox{\phantom{N}}}

\newcommand{\verofalso}{\fbox{V}\hspace{4ex}\fbox{F}}

\newcommand{\sino}{\fbox{\,Sì\,}\hspace{4ex}\fbox{\,No\,}}

\newcommand{\result}[1]{\phantom{.}\hfill \(\quadra{#1}\)}

\newcommand{\bresult}[1]{\phantom{.}\hfill \(\quadra{\emptybox~ #1}\)}

\newcommand{\intestazioneREMOVE}[2]{
  \renewcommand{\folder}{#1}
  \graphicspath{{\folder}}
  \include{\folder #2}
  \cleardoublepage  % TODO non deve esserci tra intestazione e colophon!!!
}

\newcommand{\myp}{~\\ [-1.5em]}

\newenvironment{htmulticols}[1]%
{\ifdefined\HCode                          % compilazione htlatex
\else
\begin{multicols}{#1}
\fi
}
{\ifdefined\HCode                          % compilazione htlatex
\else
\end{multicols}
\fi
}


\newcommand{\httab}{
\ifdefined\HCode
~~~~
\else
\tab
\fi
}

\newcommand{\affiancati}[4]{
  % Esempio di chiamata:
  % \affiancati{.48}{.48}{testo1}{testo2}
  \def \larga{#1}
  \def \largb{#2}
  \noindent\begin{minipage}{\larga\textwidth}
    #3
  \end{minipage}
  \hfill
  \begin{minipage}{\largb\textwidth}
    #4
  \end{minipage}
}

\newcommand{\affiancatic}[4]{
  % Esempio di chiamata:
  % \affiancati{.48}{.48}{testo1}{testo2}
  \def \larga{#1}
  \def \largb{#2}
  \def \contenutoa{#3}
  \def \contenutob{#4}
  \noindent \begin{minipage}{\larga\textwidth}
    \begin{center} \contenutoa \end{center}
  \end{minipage}
  \hfill
  \begin{minipage}{\largb\textwidth}
    \begin{center} \contenutob \end{center}
  \end{minipage}
}

\newcommand{\segnoretta}[3]{% Esempio di chiamata:
  \def \ta{#1}
  \def \tb{#2}
  \def \segno{#3}
 \begin{minipage}{.45\textwidth}
  E.A.:~\(\ta\)
 \end{minipage}
 \begin{minipage}{.25\textwidth}
  F.A.:~\(\tb\)
 \end{minipage}
 \begin{minipage}{.3\textwidth}
  \segno
 \end{minipage}
}

\newcommand{\insiemesoluzione}[3]{
  \def \grafica{#1}
  \def \predicati{#2}
  \def \parentesi{#3}
\noindent\begin{minipage}[t]{.29\textwidth}
 \begin{center}
  Forma grafica\\ %[-.5em]
     \grafica
  \vspace{.4em}
 \end{center}
  \end{minipage}
  \begin{minipage}[t]{.30\textwidth}
  \begin{center}
  Esp. con i predicati\\ [.9em]
  \predicati
 \end{center}
  \end{minipage}
  \begin{minipage}[t]{.30\textwidth}
 \begin{center}
  Esp. con le parentesi\\ [.9em]
  \parentesi
 \end{center}
  \end{minipage}
}

\newcommand{\elenconumerato}[2]{
  % Esempio di chiamata:
  % \elenconumerato{{$3+2$, $4 \cdot 5$, {con, VIRGOLE, $5^3$}}{\vspace{1cm}}}
\begin{enumerate}
 \foreach \x in #1 {\item \x #2}
\end{enumerate}
}

\newcommand{\image}[2]{% se è definita \tikzout esegue #1 altrimenti #2
  % esempio: \image{\puntigrafico}{img/puntigrafico.png}
  \ifx\tikzout\undefined
    #1
  \else
    \includegraphics[scale=1]{#2}
  \fi
}

\newcommand{\centra}[1]{\begin{center}#1\end{center}}

\newcommand{\rb}[2][-.5]{\raisebox{#1em}{\(#2\)}}

\newcommand{\prb}[2][-.5]{\raisebox{#1em}{\phantom{\(#2\)}}}

\newcommand{\ph}[1]{\phantom{#1}}

\newcommand{\sol}[1]{#1}

\newcommand{\psol}[1]{}

%% 
%%  This file may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public Licence, either
%%  version 1.3c of this licence or (at your option) any later
%%  version. The latest version of this licence is in:
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%%  and version 1.3c  or later is part of all distributions of
%%  LaTeX version 2008/05/04 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%%  and consists of the files listed in the README file.
%% 
%%
%% End of file `matdolce.cls'.
