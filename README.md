# README #

### Cosa contiene ###

Questo repository contiene i test per l'uso di make4ht.

### Come funziona ###

Nella dir tests sono presenti svariate sottodir 
una per ogni problema incontrato.

Ogni sottodir contiene un MWE che riproduce il problema.

Molti di questi problemi sono stati riportati al manutentore di make4ht 
e sono stati risolti.

Il sito di riferimento è:
[make4ht issues](https://github.com/michal-h21/make4ht/issues/)
