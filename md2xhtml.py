#!/usr/bin/env python
# -*- coding: utf-8 -*-
#------------------------------python----------------------md2xhtml.py--#
#                                                                       #
#        Traduce Matematica Dolce in formato html accessibile           #
#                                                                       #
# Copyright (c) 2021 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#--Daniele Zambelli-------------GPL-------------------------------2021--#

"""
Converte Matematica Dolce in formato html accessibile.

Assume che la cartella matematicadolce sia nella stessa directory di
md2xhtml.py.

Il risultato dell'elaborazione si trova all'interno della dir: dist.

I passi svolti dal programma sono:

* Individuazione dei volumi e dei capitoli di ogni volume.
* Creazione dell'albero md_xhtml all'interno di dist.
* Creazione delle immagini .png necessarie.
* Modifica del sorgente .tex.
* Compilazione da .tex a .html con pandoc.

"""

import os, sys
import shutil
import logging
import errno

#Creating and Configuring Logger
##Log_Format = "%(levelname)s %(asctime)s - %(message)s"
##Log_Format = ('[%(asctime)s] %(levelname)s '
##              '[%(filename)s.%(funcName)s:%(lineno)d] '
##              '%(message)s', datefmt='%a, %d %b %Y %H:%M:%S')
Log_Format = ('%(levelname)s: '
              '[%(filename)s.%(funcName)s:%(lineno)d] '
              '%(message)s')
logging.basicConfig(
##                    filename = "logfile.log",
                    stream = sys.stdout, 
                    filemode = "w",
                    format = Log_Format, 
                    level = logging.DEBUG)
logger = logging.getLogger()

NEWLINE = '\n'
CURDIR = os.curdir
CANTDIR = os.path.join(CURDIR, 'matematicadolce/cantiere')
DEPDIR = os.path.join(CURDIR, 'matematicadolce/deposito')
MATDIR = os.path.join(DEPDIR, 'materiali/')
DISTDIR = os.path.join(CURDIR, 'dist')
##PNGDIR = os.path.join(CURDIR, 'deposito', 'materiali')

TEXDOCUMENT = r"""% tex document to create .png
\newcommand{{\depdir}}{{sorgentedeposito/}}
\newcommand{{\magdir}}{{\depdir magazzino/}}
\newcommand{{\matdir}}{{\depdir materiali/}}

% \documentclass{{report}}
\documentclass[crop,
               margin={{-15mm, 10mm, 15mm, -5mm}},
               convert={{outfile=\jobname.png}}]{{standalone}}
% \documentclass[crop,
%                margin={{-15mm, 10mm, 15mm, -5mm}},
%                convert={{outext=.svg,
%                         command=\unexpanded{{pdf2svg \infile\space\outfile}}}},
%                multi=false]{{standalone}}[2012/04/13]

%========================
% Lettura preambolo
%========================
\input{{\magdir packages}}

%========================
% Lettura definizioni
%========================
\input{{\magdir definizioni}}
\input{{\magdir definizioni_tikz}}

\newcommand{{\dir}}{{{folder}}}
\input{{\dir /{graphicfile}}}
\begin{{document}}
{funct}
\end{{document}}
"""

##def hello_logger():
##    logger.info("Hello info")
##    logger.critical("Hello critical")
##    logger.warning("Hello warning")
##    logger.debug("Hello debug")

def elencocapitoli(volume):
    """Restituisce la lista dei capitoli di volume."""
    result = []
    with open(volume) as gf:
        cercanome = False
        for line in gf:
            line = line.strip()
            if cercanome:
                cercanome = False
                a_nomefile = line.find('}')
                nomefile = line[1:a_nomefile]
                capitolo = os.path.join(capitolo, nomefile)
                result.append(capitolo)
            elif line.startswith(r'\capitolo{'):
                da, a = line.index('{')+1, line.index('}')
                capitolo = line[da:a]
                capitolo = capitolo.replace(r'\matdir ', MATDIR)
                da_nomefile = line.find('{', a+1)
                if da_nomefile == -1:
                    cercanome = True
                else:
                    a_nomefile = line.find('}', da_nomefile)
                    nomefile = line[da_nomefile+1:a_nomefile]
                    capitolo = os.path.join(capitolo, nomefile)
                    result.append(capitolo)
    return result

def calcolanomi(volume):
    """Restituisce:
* il nome del volume,
* il nome della dir in cui si trova il volume
* il nome della dir dove verrà scritto
il risultato html."""
    nomevoldir, nomevol = os.path.split(volume)
    nomevol, _ = os.path.splitext(nomevol)
    nomevoldir = nomevoldir[len(CANTDIR)+1:]
    nomedistdir = os.path.join(DISTDIR, nomevoldir)
    return nomevol, nomevoldir, nomedistdir

'''
 os.path.getmtime(path)

    Return the time of last modification of path.
    The return value is a floating point number giving the number of seconds
    since the epoch (see the time module).
    Raise OSError if the file does not exist or is inaccessible.

'''

def creavolume(nomedistdir):
    """Se non è presente, crea la dir del volume."""
    try:
        os.makedirs(nomedistdir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def graphics_functions(cap_dir, graphic_file):
    """Return list of grafics functions in g_f file."""
    result = []
    with open(os.path.join(cap_dir, graphic_file)) as gf: 
        for line in gf: 
            inewcommand = line.find(r'\newcommand{')
            if inewcommand >= 0 and inewcommand < line.find(r'%'):
                result.append(line[12:line.find('}')])
    return result

def used_functions(graphicsfunctions, cap_dir, tex_file, ese_file):
    """Return list of grafics functions used in tex_file or in ese_file."""
    textests = [] 
    for t_f in [tex_file, ese_file]:
        with open(os.path.join(cap_dir, t_f)) as tf:
            textests.append(tf.read())
    textest = '\n'.join(textests)
    return [n_f for n_f in graphicsfunctions if textest.find(n_f) >= 0]

def creapng(cap_dir, tex_file, ese_file, graphic_file, png_dir, nomedistdir):
    """Se non esiste o è più vecchia crea la dir e la riempie con i png."""

##def tikz_to_png(filename, texdir, pngdir=PNGDIR):
##    """read tikz file e write png file."""
##    print(f"From: {texdir}\n")
    outdir = os.path.join(nomedistdir, png_dir)
    logger.debug(outdir)
    pdfdir = os.path.join(outdir, 'pdf') #???
##    print(f"to : {pngdir}\n")
##    print(f"and: {pdfdir}\n")
##    texfiles = [n_f for n_f in os.listdir(texdir) if n_f.endswith('.tex')]
##    graphicsfiles = [n_f for n_f in texfiles if n_f.endswith('_grafici.tex')]
##    for g_f in graphicsfiles:
##        texfiles.remove(g_f)
##    print(f"texfiles: {texfiles}\n")
##    print(f"graphicsfiles: {graphicsfiles}\n")
    graphicsfunct = graphics_functions(cap_dir, graphic_file)
    logger.debug('\n'.join(graphicsfunct))
    logger.debug(len(graphicsfunct))
    '''
Leggere riga per riga i due file (teoria e esercizi):
  estrarre i comandi grafici
  metterli nel TEXDOCUMENT
  compilare
  mettere il .png nella sua dir.
'''
##    print(f"graphicsfunctions: {graphicsfunctions}\n")
    usedfunct = used_functions(graphicsfunct, cap_dir, tex_file, ese_file)
    logger.debug('\n'.join(usedfunct))
    logger.debug(len(usedfunct))
    pippo
##    print(f"usedfunctions: {usedfunctions}\n")
##    shutil.rmtree(builddir, ignore_errors=True)
##    os.makedirs(builddir, mode=0o777, exist_ok=True)
##    shutil.rmtree(PNGDIR, ignore_errors=True)
    if not os.path.isdir(PNGDIR):
        shutil.copytree(MATDIR, PNGDIR)
    if not os.path.isdir(pngdir):
        os.mkdir(pngdir)
    if not os.path.isdir(pdfdir):
        os.mkdir(pdfdir)
##    for n_fu in TESTIMMAGINI:
    for n_fu in usedfunctions:
        n_fa_png = os.path.join(pngdir, n_fu[1:]+'.png')
        n_fa_pdf = os.path.join(pdfdir, n_fu[1:]+'.pdf')
##        print(f"n_fu: {n_fu}\n")
##        print(f"\n n_fa_pdf: {n_fa_pdf}\n")
##        textext = TEXDOCUMENT.format(texdir, n_fu)
        textext = TEXDOCUMENT.format(folder=texdir,
                                     graphicfile=graphicsfiles[0],
                                     funct=n_fu)
##        print(f"textext: {textext}\n")
        print(f'{n_fu} \t tex', end='\t')
        o_f = open('standalonepng.tex', 'w')
        o_f.write(textext)
        o_f.close()
        print('pdf \t png', end='\t')
        os.system('pdflatex -shell-escape standalonepng.tex')
        print(f'move')
        os.system(f'mv standalonepng.png {n_fa_png}')
        os.system(f'mv standalonepng.pdf {n_fa_pdf}')

def copiatex(nomefilei, tex_file, nomedistdir):
    """Se non esiste o è più vecchia crea la dir e la riempie con i png."""
    nomedir, nomefile = os.path.split(nomefilei)
    shutil.copy(nomefilei, nomedistdir)
    logger.info(f"Copiato: {nomefile}")

def elaboracapitoli(volume, capitoli, nomedistdir):
    """Elabora ogni singolo capitolo mettendo i risultati nella dir:
dist/volume.
1. Crea dist/volume se non c'è già.
2. Per ogni capitolo:
Se dist/volume o se i file lì presenti sono più vecchi:
2.1. trasforma i grafici in png e li salva nella sottodir:
     <nomecapitolo>_files;
2.2. modifica <nomecapitolo>.tex e lo salva in dist/volume;
"""
    creavolume(nomedistdir)
    for capitolo in capitoli:
        cap_dir, nomefile = os.path.split(capitolo)
        graphic_file = nomefile + '_grafici.tex'
        tex_file = nomefile + '.tex'
        ese_file = nomefile + '_ese.tex'
        graphic_file = nomefile + '_grafici.tex'
        png_dir = nomefile + '_files'
        logger.debug('; '.join((cap_dir, graphic_file, tex_file,
                                ese_file, png_dir)))
        creapng(cap_dir, tex_file, ese_file, graphic_file, png_dir,
                nomedistdir)
        copiatex(capitolo + '.tex', tex_file, nomedistdir)


def main():
    volume = './matematicadolce/cantiere/licei/v_5/m_d_licei_21_5.tex'
    logger.info(f"volume = {volume}")
    nomevol, nomevoldir, nomedistdir = calcolanomi(volume) # nomevoldir???
    logger.info(f'nomevol: {nomevol}')
    logger.info(f'nomevoldir: {nomevoldir}')
    logger.info(f'nomedistdir: {nomedistdir}')
    capitoli = elencocapitoli(volume)
    logger.info(f"capitoli: \n{NEWLINE.join(capitoli)}")
    elaboracapitoli(volume, capitoli, nomedistdir)

if __name__ == "__main__":
##    hello_logger()
    main()




'''
##TESTIMMAGINI = ['\\continuitagraficoese', '\\continuitagraficoa',
##                '\\continuitaintervallo', '\\continuitasincosa']
##TESTIMMAGINI = ['\\continuitagraficoa',
##                '\\continuitaintervallo']
##print(f"TESTIMMAGINI: {TESTIMMAGINI}\n")

tikz_to_png("funzioni",
            os.path.join(DEPDIR, "materiali", "12_Analisi",
                         "01_Topologia_Funzioni", "funzioni02"))
            
'''
'''
# Main program
#####
# Translate all file tikz in PGFDIR (lbr)
# to file png stored in PNGDIR (pngimg).
###
for pgffile in [n_f[:-4] for n_f in os.listdir(PGFDIR)
                if n_f.startswith('fig') and n_f.endswith('.pgf')]:
    pgf2png(filename=pgffile)
    os.system('rm standalonepng.*')

#####
# Translate all file png in PNGDIR (pngimg)
# to file stl stored in STLDIR (stlimg),
# then all file stl in file gcode stored in GCODEDIR (gcode).
###
for pngfile in [n_f[:-4] for n_f in os.listdir(PNGDIR) if n_f[-4:] == '.png']:
    png2gcode(filename=pngfile)
'''


